#ifndef CPP_CHATBOT_INTERFACES_INITIALIZATION_AWARE_HPP
#define CPP_CHATBOT_INTERFACES_INITIALIZATION_AWARE_HPP

#include <string>
#include "Di.hpp"

using DiInterface = TwistersFury::Shared::Di::Interfaces::Di;

namespace TwistersFury::Shared::Di::Interfaces {
    struct InitializationAware {
        virtual void initialize() =0;
    };
}

#endif //CPP_CHATBOT_INTERFACES_INITIALIZATION_AWARE_HPP
