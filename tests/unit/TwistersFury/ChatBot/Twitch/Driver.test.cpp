#include "catch.hpp"
#include "ChatBot/Twitch/Driver.hpp"

using TwistersFury::ChatBot::Twitch::Driver;

TEST_CASE("TwistersFury::ChatBot::Twitch::Driver") {
    auto testSubject = Driver{};

    SECTION("It Connects Properly") {
        REQUIRE(true == testSubject.connect());
    }
}