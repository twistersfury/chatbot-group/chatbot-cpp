#define CATCH_CONFIG_MAIN

#include <catch.hpp>
#include <fakeit.hpp>
#include <Shared/Kernel.hpp>

using namespace fakeit;

using TwistersFury::Shared::Kernel;

TEST_CASE("TwistersFury::Shared::Kernel") {
    SECTION("It Handles The Request") {
        Mock<DiInterface> diMocker;

        auto &diInstance = diMocker.get();

        auto kernel = Kernel{diInstance};
        REQUIRE(kernel.handle(0, nullptr) == "Hello World!!!!");
    }
}