#ifndef CPP_CHATBOT_KERNEL_HPP
#define CPP_CHATBOT_KERNEL_HPP

#include <string>

#include "Shared/Di/Interfaces/Di.hpp"

using std::string;
using DiInterface = TwistersFury::Shared::Di::Interfaces::Di;

namespace TwistersFury::Shared {
    class Kernel {
    public:
        explicit Kernel(DiInterface &diInstance);

        string handle(int argc, char *argv[]);
    private:
        DiInterface &diInterface;
    };
}

#endif //CPP_CHATBOT_KERNEL_HPP
