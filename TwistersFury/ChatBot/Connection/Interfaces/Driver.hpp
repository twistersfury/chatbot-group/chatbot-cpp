//
// Created by Fenikkusu on 3/7/23.
//

#ifndef CPP_CHATBOT_DRIVER_INTERFACE_HPP
#define CPP_CHATBOT_DRIVER_INTERFACE_HPP

namespace TwistersFury::ChatBot::Connection::Interfaces {
    struct Driver {
        virtual bool connect() =0;
    };
}

#endif //CPP_CHATBOT_DRIVER_INTERFACE_HPP
