#include "catch.hpp"
#include "ChatBot/Di/FactoryDefault.hpp"

using TwistersFury::ChatBot::Di::FactoryDefault;

TEST_CASE("TwistersFury::ChatBot::Di::FactoryDefault") {
    SECTION("It Lazy Loads Correctly") {
        auto di = &FactoryDefault::getDi();

        di->setReady(true);

        REQUIRE(true == FactoryDefault::getDi().isReady());
    }
}