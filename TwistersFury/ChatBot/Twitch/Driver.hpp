//
// Created by Fenikkusu on 3/7/23.
//

#ifndef CPP_CHATBOT_DRIVER_HPP
#define CPP_CHATBOT_DRIVER_HPP

#include <ChatBot/Connection/Interfaces/Driver.hpp>

using DriverInterface = TwistersFury::ChatBot::Connection::Interfaces::Driver;

namespace TwistersFury::ChatBot::Twitch {
    class Driver : public DriverInterface {
    public:
        bool connect() override;
    };
};

#endif //CPP_CHATBOT_DRIVER_HPP
