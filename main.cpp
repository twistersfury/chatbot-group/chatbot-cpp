#include <iostream>
#include "TwistersFury/ChatBot/Kernel.hpp"

using TwistersFury::ChatBot::Kernel;

int main(int argc, char *argv[]) {
    auto kernel = Kernel{};

    std::cout << kernel.handle(argc, argv) << std::endl;

    return 0;
}
