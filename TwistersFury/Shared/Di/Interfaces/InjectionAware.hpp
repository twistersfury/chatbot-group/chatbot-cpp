#ifndef CPP_CHATBOT_INTERFACES_INJECTION_AWARE_HPP
#define CPP_CHATBOT_INTERFACES_INJECTION_AWARE_HPP

#include <string>
#include "Di.hpp"

using DiInterface = TwistersFury::Shared::Di::Interfaces::Di;

namespace TwistersFury::Shared::Di::Interfaces {
    struct InjectionAware {
        virtual InjectionAware& setDi(DiInterface diInstance) =0;
        virtual DiInterface getDi() =0;
    };
}

#endif //CPP_CHATBOT_INTERFACES_INJECTION_AWARE_HPP
