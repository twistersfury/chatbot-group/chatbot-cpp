#ifndef CPP_CHATBOT_FACTORY_DEFAULT_HPP
#define CPP_CHATBOT_FACTORY_DEFAULT_HPP

namespace TwistersFury::ChatBot::Di {
    class FactoryDefault {
    public:
        ~FactoryDefault();

        // Deleting Standard Operators
        FactoryDefault(const FactoryDefault& other) = default;
        FactoryDefault& operator=(const FactoryDefault& other) = delete;
        FactoryDefault(FactoryDefault&& other) = delete;
        FactoryDefault& operator=(FactoryDefault&& other) = delete;

        // Singleton Method
        static FactoryDefault& getDi();

        [[nodiscard]] bool isReady() const;
        FactoryDefault& setReady(bool ready);
    private:
        FactoryDefault();

        bool ready = false;
    };
}

#endif //CPP_CHATBOT_FACTORY_DEFAULT_HPP
