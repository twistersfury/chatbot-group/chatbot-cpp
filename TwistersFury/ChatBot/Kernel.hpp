#ifndef INTERVIEW_CPP_KERNEL_HPP
#define INTERVIEW_CPP_KERNEL_HPP

#include <string>
#include <Shared/Kernel.hpp>
#include <ChatBot/Di/FactoryDefault.hpp>

using std::string;

namespace TwistersFury::ChatBot {
    class Kernel : public TwistersFury::Shared::Kernel {
    };
};

#endif //INTERVIEW_CPP_KERNEL_HPP
