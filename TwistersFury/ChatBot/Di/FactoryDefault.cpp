#include <cassert>

#include "FactoryDefault.hpp"

namespace {
    thread_local TwistersFury::ChatBot::Di::FactoryDefault* _instance = nullptr;
}

namespace TwistersFury::ChatBot::Di {
    FactoryDefault::FactoryDefault() {
        assert(!_instance);
        _instance = this;
    };

    FactoryDefault::~FactoryDefault() {
        _instance = nullptr;
    };

    FactoryDefault& FactoryDefault::getDi() {
        if (nullptr == _instance) {
            _instance = new FactoryDefault();
        }

        return *_instance;
    }

    bool FactoryDefault::isReady() const {
        return ready;
    }

    FactoryDefault& FactoryDefault::setReady(bool setReady) {
        ready = setReady;

        return *this;
    }
}