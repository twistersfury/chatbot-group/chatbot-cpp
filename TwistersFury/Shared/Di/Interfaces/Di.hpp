#ifndef CPP_CHATBOT_INTERFACES_DI_HPP
#define CPP_CHATBOT_INTERFACES_DI_HPP

#include <string>

using std::string;

namespace TwistersFury::Shared::Di::Interfaces {
    // Nothing To See, Just Yet..
    struct Di {
        virtual ~Di() =0;
    };
}

#endif //CPP_CHATBOT_INTERFACES_DI_HPP
